<?php


namespace SonarTestVendor;


class App
{
    public static function getDbConnection()
    {
        $dsn = 'this_is_just_for_test';
        return new \PDO($dsn);
    }

}